
#include <iostream>
#include "graficos_txt.h"

using namespace std;

void CrearVentana(int ancho, int alto, const char *titulo)
{
    cout << "Acabo de crear una ventana de " << ancho << " por " << alto
         << " píxeles llamada " << titulo << endl;
}

void Punto(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
    cout << "He dibujado un punto en (" << x << "," << y
         << ") de color (" << static_cast<int>(r) << "," << static_cast<int>(g)
         << "," << static_cast<int>(b) << ")" << endl;
}

void Linea(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b)
{
    cout << "He dibujado una línea desde el punto (" << x1 << "," << y1 << ")"
         << " hasta el punto (" << x2 << "," << y2 << ")" << " de color ("
         << static_cast<int>(r) << "," << static_cast<int>(g) << ","
         << static_cast<int>(b) << ")" << endl;

}

void Circulo(int x, int y, int rad, unsigned char r, unsigned char g, unsigned char b)
{
    cout << "He dibujado un círculo centrado en (" << x << "," << y << ")"
         << " de radio " << rad<< " de color (" << static_cast<int>(r) << ","
         << static_cast<int>(g) << "," << static_cast<int>(b) << ")" << endl;
}

void Rectangulo(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b)
{
    cout << "He dibujado un rectángulo con esquinas (" << x1 << "," << y1 << ")"
         << " y (" << x2 << "," << y2 << ")" << " de color (" << static_cast<int>(r)
         << "," << static_cast<int>(g) << "," << static_cast<int>(b) << ")" << endl;
}
