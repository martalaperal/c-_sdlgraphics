//
//  rectangulo.h
//    Este módulo implementa un TDA rectángulo para representar rectángulos
//    en un espacio 2D
//

#ifndef __RECTANGULO__H__
#define __RECTANGULO__H__

#include "punto.h"

struct Rectangulo2D {
    Punto2D p[2];
};

// Un rectángulo está definido por dos puntos (esquinas opuestas)
//  P1             P3
//   *-------------*
//   |             |
//   |             |
//   |             |
//   |             |
//   |             |
//   |             |
//   *-------------*
//  P4             P2
// Para definir este rectángulo debemos dar P1 y P2 o bien P3 y P4


// Crear un rectángulo a partir de dos puntos
void SetRectangulo(Rectangulo2D &rec, Punto2D p1, Punto2D p2);

// Obtener los puntos que definen un rectángulo:
// El primer punto es la esquina superior izquierda y el segundo la esquina
// inferior derecha.
Punto2D GetRectanguloEsquina1(Rectangulo2D rec);
Punto2D GetRectanguloEsquina2(Rectangulo2D rec);
Punto2D GetRectanguloEsquina3(Rectangulo2D rec);
Punto2D GetRectanguloEsquina4(Rectangulo2D rec);

double BaseRectangulo(Rectangulo2D rec);
double AlturaRectangulo(Rectangulo2D rec);
double AreaRectangulo(Rectangulo2D rec);
void LeeRectangulo(Rectangulo2D &rec);
void EscribeRectangulo(Rectangulo2D rec);

#endif
