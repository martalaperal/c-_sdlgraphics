
#include <iostream>
#include <cmath>
#include "punto.h"

using namespace std;

void SetPunto(Punto2D &p, double x, double y)
{
    p.x = x;
    p.y = y;
}

double GetPuntoX(Punto2D p)
{
    return p.x;
}

double GetPuntoY(Punto2D p)
{
    return p.y;
}

double Distancia(Punto2D p1, Punto2D p2)
{
    double x=p1.x-p2.x;
    double y=p1.y-p2.y;
    return sqrt(x*x+y*y);
}

void LeePunto(Punto2D &p)
{
    // El formato para introducir un punto será (x,y)
    char car;
    cin >> car >> p.x >> car >> p.y >> car;
}

void EscribePunto(Punto2D p)
{
    cout << "(" << p.x << "," << p.y << ")";
}
