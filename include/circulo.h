//
//  circulo.h
//    Este módulo implementa un TDA círculo para representar círculos en un
//    espacio 2D
//
#ifndef __CIRCULO__H__
#define __CIRCULO__H__

#include "punto.h"

struct Circulo2D {
    Punto2D centro;
    double radio;
};

// Asignar el centro y el radio al círculo cir
void SetCirculo(Circulo2D &cir, Punto2D c, double r);

// Obtener el centro y el radio de un círculo dado
Punto2D GetCirculoCentro(Circulo2D cir);
double GetCirculoRadio(Circulo2D cir);

// Calcular el área de un círculo
double AreaCirculo(Circulo2D cir);

// Leer o escribir los datos de un círculo por consola (cin y cout)
void LeeCirculo(Circulo2D &cir);
void EscribeCirculo(Circulo2D cir);

#endif
