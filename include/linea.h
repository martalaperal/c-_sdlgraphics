//
//  linea.h
//    Este módulo implementa un TDA línea para representar líneas en un
//    espacio 2D
//

#ifndef __LINEA__H__
#define __LINEA__H__

#include "punto.h"

struct Linea2D {
    Punto2D p[2];
};

// Asignar los puntos que definen una línea
void SetLinea(Linea2D &lin, Punto2D p1, Punto2D p2);

// Obtener los puntos que definen una línea
Punto2D GetLineaPunto1(Linea2D lin);
Punto2D GetLineaPunto2(Linea2D lin);

// Calcular la distancia euclídea entre los dos puntos que definen la línea
double LongitudLinea(Linea2D lin);

// Leer o escribir los datos de una línea por consola (cin y cout)
void LeeLinea(Linea2D &lin);
void EscribeLinea(Linea2D lin);

#endif
