
#include <iostream>
#include "punto.h"
#include "linea.h"
#include "rectangulo.h"

using namespace std;

// Macro que devuelve el máximo de dos valores
#define MAX(x,y) (((x)>(y)) ? (x) : (y))
#define MIN(x,y) (((x)<(y)) ? (x) : (y))


void SetRectangulo(Rectangulo2D &rec, Punto2D p1, Punto2D p2)
{
    // Dados los dos puntos p1 y p2 calculo cuales serán las esquinas para definir el rectángulo
    // Cada esquina estará formada por dos puntos
    Punto2D esq1,esq2;
    //SetPunto(puntoamodificar,coordenada x,coordenada y);
    SetPunto(esq1,MIN(GetPuntoX(p1),GetPuntoX(p2)),MIN(GetPuntoY(p1),GetPuntoY(p2)));
    SetPunto(esq2,MAX(GetPuntoX(p1),GetPuntoX(p2)),MAX(GetPuntoY(p1),GetPuntoY(p2)));
    rec.p[0] = esq1; // esq1 es la esquina superior izquierda
    rec.p[1] = esq2; // esq2 es la esquina inferior derecha
}

Punto2D GetRectanguloEsquina1(Rectangulo2D rec)
{
    return rec.p[0];
}

Punto2D GetRectanguloEsquina2(Rectangulo2D rec)
{
    return rec.p[1];
}

Punto2D GetRectanguloEsquina3(Rectangulo2D rec)
{
    Punto2D aux;
    SetPunto(aux,GetPuntoX(rec.p[1]),GetPuntoY(rec.p[0]));
    return aux;
}

Punto2D GetRectanguloEsquina4(Rectangulo2D rec)
{
    Punto2D aux;
    SetPunto(aux,GetPuntoX(rec.p[0]),GetPuntoY(rec.p[1]));
    return aux;
}

double BaseRectangulo(Rectangulo2D rec)
{
    return GetPuntoX(rec.p[1])-GetPuntoX(rec.p[0]);
}

double AlturaRectangulo(Rectangulo2D rec)
{
    return GetPuntoY(rec.p[0])-GetPuntoY(rec.p[1]);
}

double AreaRectangulo(Rectangulo2D rec)
{
	return BaseRectangulo(rec) * AlturaRectangulo(rec) ;
}

void LeeRectangulo(Rectangulo2D &rec)
{
    // El formato de introducción de un rectángulo es (x1,y1)-(x2,y2)
    char guion;
    LeePunto(rec.p[0]);
    cin >> guion;
    LeePunto(rec.p[1]);
}

void EscribeRectangulo(Rectangulo2D rec)
{
    EscribePunto(rec.p[0]);
    cout << "-";
    EscribePunto(rec.p[1]);
}
