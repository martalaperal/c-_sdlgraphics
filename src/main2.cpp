//
// Programa que lee dos puntos y dibuja un rectángulo con un circulo en cada
// esquina.
// El radio de los círculos se preguntará por cin
//   ...           ...
// .  P1 .       .     .
// .  *--.-------.--|  .
// .  |  .       .  |  .
//   ...           ...
//    |             |
//    |             |
//   ...           ...
// .  |  .       .  |  .
// .  |--.-------.--*  .
// .     .       .  P2 .
//   ...           ...

#include <iostream>
#include "formas.h"
#include "graficos.h"
#include "dibujante.h"

using namespace std;

int main(int argc, char *argv[]){

	Punto2D p1,p2 ;
	Rectangulo2D r ;
	Circulo2D c1, c2, c3, c4 ;
	double rad ;

	cout << "Se va a utilizar una ventana de tamaño 200x200, procura no "
		<< "salirte de esas dimensiones" << endl;
	cout << "Dime las coordenadas del primer punto: ";
	LeePunto(p1);
	cout << "Dime las coordenadas del segundo punto: ";
	LeePunto(p2);

	SetRectangulo(r,p1,p2);

	cout << "\nDime el radio de los circulos : " ;
	cin >> rad ;
	SetCirculo(c1, GetRectanguloEsquina1(r), rad);
	SetCirculo(c2, GetRectanguloEsquina2(r), rad);
	SetCirculo(c3, GetRectanguloEsquina3(r), rad);
	SetCirculo(c4, GetRectanguloEsquina4(r), rad);

	CrearVentana(200,200,"Ejemplo");
	DibujarRectangulo(r,AMARILLO);
	DibujarCirculo(c1, MAGENTA) ;
	DibujarCirculo(c2, MAGENTA) ;
	DibujarCirculo(c3, MAGENTA) ;
	DibujarCirculo(c4, MAGENTA) ;
	
	cout << "\nPulsa una tecla para terminar\n" ;
	cin.get();
	cin.get();
}
