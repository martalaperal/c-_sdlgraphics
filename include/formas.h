//
//  formas.h
//    Fichero de cabecera para la biblioteca que aglutina a los cuatro módulos
//    para crear formas 2D (punto, linea, rectangulo y circulo).
//

#ifndef __FORMAS__H__
#define __FORMAS__H__

#include "punto.h"
#include "linea.h"
#include "circulo.h"
#include "rectangulo.h"

#endif
