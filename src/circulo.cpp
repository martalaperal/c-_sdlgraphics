
#include <iostream>
#include <cmath>
#include "circulo.h"

using namespace std ;

void SetCirculo(Circulo2D &cir, Punto2D c, double r){

	cir.centro.x=c.x ;
	cir.centro.y=c.y ;
	cir.radio=r ;
}


Punto2D GetCirculoCentro(Circulo2D cir){

	return cir.centro ;
}


double GetCirculoRadio(Circulo2D cir){

	return cir.radio ;
}


double AreaCirculo(Circulo2D cir){

	return M_PI*(pow(cir.radio,2));   // M_PI corresponde al número pi
}


void LeeCirculo(Circulo2D &cir){

	//char car ;
	//cin >> car >> cir.centro.x >> car >> cir.centro.y ;
	LeePunto(cir.centro); // Esta forma es mejor
	double rad ;
	cin >> rad >> cir.radio ;
}	



void EscribeCirculo(Circulo2D cir){

	EscribePunto(cir.centro) ;
	cout << "\nRadio: " << cir.radio ;
}
