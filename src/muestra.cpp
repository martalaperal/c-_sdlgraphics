// 	
// 	ESTIMACIÓN DEL NÚMERO PI
//	Programa que calcula un valor aproximado de Pi a partir de números aleatorios
//

#include <iostream>

// Incluimos estas bibliotecas para dibujar :

#include "formas.h"		
#include "graficos.h"
#include "dibujante.h"

// Incluimos estas bibliotecas para crear números pseudoaleatorios ( rand() / srand() )
// mediante el tiempo actual del sistema ( time() ) :

#include <cstdlib>
#include <ctime>

using namespace std ;

int Entero_Aleatorio ( int min, int max ) ;
double Nums_Aleatorios( int npuntos, Punto2D cen ) ;

int main(int argc, char *argv[]){

	// PARTE TEÓRICA

	double pi ;
	// El usuario dará en la línea de ordenes el número de puntos aleatorios a crear
	// Mediante la función "atoi()" transformaremos el número de tipo char* a int
	int puntos=atoi(argv[1]);
	
		
	// PARTE GRÁFICA

	// Creamos :
	//	- Círculo 2D
	//	- Rectángulo 2D que tomará forma de cuadrado
	//	- Dos puntos 2D que determinarán las esquinas del cuadrado
	//	- Centro de la ventana/círculo/cuadrado con un punto 2D

	Circulo2D cir ;
	Rectangulo2D rec ;
	Punto2D centro, esq1, esq2 ;

	// Establecemos las características de cada una de las formas según lo pedido
	
	SetPunto(esq1,50,50) ;
	SetPunto(esq2,450,450) ;
	SetRectangulo(rec,esq1,esq2) ;
	SetPunto(centro,250,250) ;	
	SetCirculo(cir,centro,200) ;

	// Ahora procedemos a representar gráficamente :
	//	- Ventana de 500x500 llamada "Numero Pi"
	//	- Cuadrado de 400x400 de color rojo
	//	- Círculo de radio 200 de color verde

	CrearVentana(500,500,"Numero Pi") ;	
	DibujarRectangulo(rec,ROJO) ;
	DibujarCirculo(cir,VERDE) ;


	// PARTE TEÓRICA Y GRÁFICA

	// Creamos números aleatorios con valor desde 50 a 450 para que no esten fuera del cuadrado
	// Será un punto 2D con tales valores tanto para la coordenada "x" como para la coordenada "y"
	// Una vez creado el punto aleatorio pasamos a evaluarlo para averiguar su posición

	pi = Nums_Aleatorios(puntos,centro) ;

	// Una vez hallado el valor aproximado de Pi, lo sacamos por pantalla

	cout << "\n\nLa aproximacion del numero Pi segun los " << puntos 
		<< " puntos aleatorios calculados es Pi = " << pi ;

	// Terminamos
	
	cout << "\n\nPulsa una tecla para terminar\n\n" ;
	cin.get();
	cin.get();

}

// Función que calcula números enteros aleatorios dentro de un rango [min,max]

int Entero_Aleatorio ( int min, int max ){

	return static_cast<int>(min + ( max - min + 1 ) * ( rand() / ( RAND_MAX + 1.0 ) )) ;
}


double Nums_Aleatorios( int npuntos, Punto2D cen ){
	
	// aproximacion = número aproximado de Pi a devolver
	double aproximacion ;

	// nc = puntos dentro del círculo
	int nc=0 ; 

	// En el punto 2D "aux" almacenaremos el número aleatorio
	Punto2D aux ;

	// Utilizamos el tiempo del sistema para calcular un valor aleatorio
	srand(time(0)) ;

	// Calculamos hasta "npuntos" aleatorios	
	for ( int i=0 ; i < npuntos ; ++i ){

		// Creamos los valores aleatorios "x" e "y" para guardarlos en "aux"
		SetPunto(aux,Entero_Aleatorio(50,450),Entero_Aleatorio(50,450)) ;

		// Si la distancia entre el "Punto2D aux" aleatorio y el centro 			
		// es menor-igual que el radio del círculo, entonces el punto está dentro de éste
		if( Distancia(aux,cen) <= 200 ){

			// Incrementamos el nº de puntos dentro del círculo
			nc++ ;

			// Dibujamos en pantalla el punto aleatorio de color verde			
			DibujarPunto(aux,VERDE) ;
		}
		
		// En caso contrario, el punto estará fuera del círculo, es decir, dentro del cuadrado
		else{			
			// Dibujamos en pantalla el punto aleatorio de color rojo 
			DibujarPunto(aux,ROJO) ;
		}

		if ( i % 100 == 0 && i>0 ){

			//Sacamos por pantalla el valor aproximado de Pi cada 100 puntos
			aproximacion = ( 4 * nc ) / ( npuntos * 1.0 ) ;
			cout << "\nEl valor aproximado de Pi con " << i 
				<< " puntos es Pi = " << aproximacion ;
		}
	}

	// Calculamos el valor aproximado de Pi y lo devolvemos mediante "return"
	aproximacion = ( 4 * nc ) / ( npuntos * 1.0 ) ;
	return aproximacion ;	
}
