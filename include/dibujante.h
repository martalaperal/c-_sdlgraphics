//
//  dibujante.h
//    Fichero de cabecera que contiene funciones específicas para dibujar las
//    formas que tenemos haciendo uso de una biblioteca básica de gráficos
//    (por ejemplo graficos_txt)
//

#ifndef __DIBUJANTE__H__
#define __DIBUJANTE__H__

#include "formas.h"
#include "graficos.h"

enum Color {BLANCO, NEGRO, ROJO, VERDE, AZUL, AMARILLO, CIAN, MAGENTA} ;

void DibujarPunto(Punto2D p, Color col) ;
void DibujarLinea(Linea2D lin, Color col) ;
void DibujarRectangulo(Rectangulo2D r, Color col) ;
void DibujarCirculo(Circulo2D c, Color col) ;

#endif
