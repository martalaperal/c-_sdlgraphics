

#include "dibujante.h"

// Matriz que almacena la paleta de color. Por cada color (8 en total)
// almacena la tripleta RGB necesaria para hacer uso de las funciones gráficas.
static const unsigned char _paleta[8][3] = {
     {255,255,255},    // Blanco
     {  0,  0,  0},    // Negro
     {255,  0,  0},    // Rojo
     {  0,255,  0},    // Verde
     {  0,  0,255},    // Azul
     {255,255,  0},    // Amarillo
     {  0,255,255},    // Cian
     {255,  0,255}     // Magenta
};

void DibujarPunto(Punto2D p, Color col)
{
    Punto(static_cast<int>(GetPuntoX(p)),static_cast<int>(GetPuntoY(p)),
          _paleta[col][0],_paleta[col][1],_paleta[col][2]);
}

void DibujarLinea(Linea2D lin, Color col)
{
    Punto2D p1, p2;
    p1 = GetLineaPunto1(lin);
    p2 = GetLineaPunto2(lin);
    Linea(static_cast<int>(GetPuntoX(p1)),static_cast<int>(GetPuntoY(p1)),
          static_cast<int>(GetPuntoX(p2)),static_cast<int>(GetPuntoY(p2)),
          _paleta[col][0],_paleta[col][1],_paleta[col][2]);
}

void DibujarRectangulo(Rectangulo2D r, Color col)
{
    Punto2D esq1,esq2;
    esq1 = GetRectanguloEsquina1(r);
    esq2 = GetRectanguloEsquina2(r);
    Rectangulo(static_cast<int>(GetPuntoX(esq1)),static_cast<int>(GetPuntoY(esq1)),
               static_cast<int>(GetPuntoX(esq2)),static_cast<int>(GetPuntoY(esq2)),
               _paleta[col][0],_paleta[col][1],_paleta[col][2]);
}

void DibujarCirculo(Circulo2D c, Color col)
{
    Punto2D cen;
    cen=GetCirculoCentro(c);
    Circulo(static_cast<int>(GetPuntoX(cen)),static_cast<int>(GetPuntoY(cen)),
            static_cast<int>(GetCirculoRadio(c)),
            _paleta[col][0],_paleta[col][1],_paleta[col][2]);
}
