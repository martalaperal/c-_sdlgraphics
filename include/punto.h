//
//  punto.h
//    Este módulo implementa un TDA punto para almacenar coordenadas 2D
//

#ifndef __PUNTO__H__
#define __PUNTO__H__

struct Punto2D {
    double x,y;     // Las coordenadas serán (x,y)
};

// Asignar al punto p las coordenadas (x,y)
void SetPunto(Punto2D &p, double x, double y);

// Obtener las coordenadas de un punto
double GetPuntoX(Punto2D p);
double GetPuntoY(Punto2D p);

// Distancia entre dos puntos
double Distancia(Punto2D p1, Punto2D p2);

// Leer las coordenadas de un punto por cin (consola)
void LeePunto(Punto2D &p);

// Escribir las coordenadas de un punto en cout (consola)
void EscribePunto(Punto2D p);

#endif
