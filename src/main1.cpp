//
// Programa que lee dos puntos y dibuja un rectángulo con un aspa que une
// ambos puntos.
//
//  P1
//   *-------------|
//   | \         / |
//   |   \     /   |
//   |     \ /     |
//   |     / \     |
//   |   /     \   |
//   | /         \ |
//   |-------------*
//                 P2

#include <iostream>
#include "formas.h"
#include "graficos.h"
#include "dibujante.h"

using namespace std;

int main(int argc, char *argv[])
{
    Rectangulo2D r;
    Punto2D p1,p2;
    Linea2D l1, l2;

    cout << "Se va a utilizar una ventana de tamaño 200x200, procura no " <<
            "salirte de esas dimensiones" << endl;
    cout << "Dime las coordenadas del primer punto: ";
    LeePunto(p1);
    cout << "Dime las coordenadas del segundo punto: ";
    LeePunto(p2);

    // Creamos las 3 formas que vamos a dibujar ...
    SetRectangulo(r,p1,p2);
    SetLinea(l1,p1,p2);
    SetLinea(l2,GetRectanguloEsquina3(r),GetRectanguloEsquina4(r));

    // Las dibujamos ...
    CrearVentana(200,200,"Ejemplo");
    DibujarRectangulo(r,AMARILLO);
    DibujarLinea(l1,VERDE);
    DibujarLinea(l2,VERDE);

    // Esperamos la pulsación de una tecla
    cout << "Estoy esperando a que pulses una tecla ..." << endl;
    cin.get();
    cin.get();
}
