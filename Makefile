
SRC = src
INC = include
OBJ = obj
LIB = lib
BIN = bin


all:  ${BIN}/main1 ${BIN}/main2 ${BIN}/muestra


# **** Compilando ****


${OBJ}/punto.o : ${SRC}/punto.cpp ${INC}/punto.h
	g++ -c ${SRC}/punto.cpp -o ${OBJ}/punto.o -I${INC}/

${OBJ}/linea.o : ${SRC}/linea.cpp ${INC}/linea.h ${INC}/punto.h
	g++ -c ${SRC}/linea.cpp -o ${OBJ}/linea.o -I${INC}/

${OBJ}/circulo.o : ${SRC}/circulo.cpp ${INC}/circulo.h ${INC}/punto.h
	g++ -c ${SRC}/circulo.cpp -o ${OBJ}/circulo.o -I${INC}/

${OBJ}/rectangulo.o : ${SRC}/rectangulo.cpp ${INC}/rectangulo.h ${INC}/punto.h
	g++ -c ${SRC}/rectangulo.cpp -o ${OBJ}/rectangulo.o -I${INC}/

${OBJ}/graficos.o : ${SRC}/graficos.cpp ${INC}/graficos.h
	g++ -c ${SRC}/graficos.cpp -o ${OBJ}/graficos.o -I${INC}/

${OBJ}/dibujante.o : ${SRC}/dibujante.cpp ${INC}/dibujante.h ${INC}/graficos.h ${INC}/formas.h
	g++ -c ${SRC}/dibujante.cpp -o ${OBJ}/dibujante.o -I${INC}/

${OBJ}/main1.o : ${SRC}/main1.cpp ${INC}/formas.h ${INC}/graficos.h ${INC}/dibujante.h
	g++ -c ${SRC}/main1.cpp -o ${OBJ}/main1.o -I${INC}/

${OBJ}/main2.o : ${SRC}/main2.cpp ${INC}/formas.h ${INC}/graficos.h ${INC}/dibujante.h
	g++ -c ${SRC}/main2.cpp -o ${OBJ}/main2.o -I${INC}/

${OBJ}/muestra.o : ${SRC}/muestra.cpp ${INC}/formas.h ${INC}/graficos.h ${INC}/dibujante.h
	g++ -c ${SRC}/muestra.cpp -o ${OBJ}/muestra.o -I${INC}/



# **** Creamos bibliotecas ****


${LIB}/libdibujante.a : ${OBJ}/dibujante.o
	ar rsv ${LIB}/libdibujante.a ${OBJ}/dibujante.o

${LIB}/libformas.a : ${OBJ}/punto.o ${OBJ}/linea.o ${OBJ}/rectangulo.o ${OBJ}/circulo.o
	ar rsv ${LIB}/libformas.a ${OBJ}/punto.o ${OBJ}/linea.o ${OBJ}/rectangulo.o ${OBJ}/circulo.o

${LIB}/libgraficos.a : ${OBJ}/graficos.o
	ar rsv ${LIB}/libgraficos.a ${OBJ}/graficos.o



# **** Enlazamos y creamos los ejecutables ****
 

${BIN}/main1: ${OBJ}/main1.o ${LIB}/libdibujante.a ${LIB}/libformas.a ${LIB}/libgraficos.a
	g++ -o ${BIN}/main1 ${OBJ}/main1.o -L${LIB} -ldibujante -lformas -lgraficos -lSDL -lSDL_gfx

${BIN}/main2: ${OBJ}/main2.o ${LIB}/libdibujante.a ${LIB}/libformas.a ${LIB}/libgraficos.a
	g++ -o ${BIN}/main2 ${OBJ}/main2.o -L${LIB} -ldibujante -lformas -lgraficos -lSDL -lSDL_gfx

${BIN}/muestra: ${OBJ}/muestra.o ${LIB}/libdibujante.a ${LIB}/libformas.a ${LIB}/libgraficos.a
	g++ -o ${BIN}/muestra ${OBJ}/muestra.o -L${LIB} -ldibujante -lformas -lgraficos -lSDL -lSDL_gfx



# **** Limpiamos códigos objeto y bibliotecas ****


clean:
	@-rm ${OBJ}/*.o
	@-rm ${LIB}/libdibujante.a ${LIB}/libformas.a ${LIB}/libgraficos.a



# **** Lo limpiamos todo (incluidos los ejecutables) ****


mrproper: clean
	@-rm ${BIN}/*


# ************************************************************************
# **** Añadimos la posibilidad de enlazar con otra biblioteca gráfica ****
# ************************************************************************

