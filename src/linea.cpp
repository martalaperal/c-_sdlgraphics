
#include <iostream>
#include <cmath>
#include "linea.h"

// using namespace std;   // Omitimos using namespace, más adelante se utilizará "std::" por tanto

void SetLinea(Linea2D &lin, Punto2D p1, Punto2D p2)
{
    lin.p[0] = p1;
    lin.p[1] = p2;
}

double LongitudLinea(Linea2D lin)
{
    double dx = GetPuntoX(lin.p[0]) - GetPuntoX(lin.p[1]);
    double dy = GetPuntoY(lin.p[0]) - GetPuntoY(lin.p[1]);
    return sqrt(dx*dx+dy*dy);
}


Punto2D GetLineaPunto1(Linea2D lin){
	
	return lin.p[0];
}



Punto2D GetLineaPunto2(Linea2D lin){

	return lin.p[1];
}


void LeeLinea(Linea2D &lin)
{
    // El formato para leer una línea es (x1,y1)-(x2,y2)
    char guion;
    LeePunto(lin.p[0]);
    std::cin >> guion;        // No hemos puesto using namespace
    LeePunto(lin.p[1]);
}

void EscribeLinea(Linea2D lin)
{
    EscribePunto(lin.p[0]);
    std::cout << "-";         // No hemos puesto using namespace
    EscribePunto(lin.p[1]);
}
