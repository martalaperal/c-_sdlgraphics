//
//  graficos_txt.h
//    Este módulo simula el dibujo de algunas formas básicas en una ventana gráfica.
//    En lugar de hacer el dibujo simplemente mostrará un mensaje en consola
//    diciendo que dibujo debería haber realizado.
//

#ifndef __GRAFICOS__TXT__H__
#define __GRAFICOS__TXT__H__

// Crear (abrir) una ventana en la pantalla de un tamaño (ancho x alto) y con un título
void CrearVentana(int ancho, int alto, const char *titulo);

// Dibujar un punto
void Punto(int x, int y, unsigned char r, unsigned char g, unsigned char b);

// Dibujar una línea entre dos puntos (x1,y1) - (x2,y2)
void Linea(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b);

// Dibujar un círculo centrado en (x,y) y de radio rad
void Circulo(int x, int y, int rad, unsigned char r, unsigned char g, unsigned char b);

// Dibujar un rectángulo entre los puntos (x1,y1) y (x2,y2)
void Rectangulo(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b);

#endif


// Nota: en las funciones de dibujo, el color siempre se da con una tripleta de
//       valores de la forma (r,g,b)
//       Los colores se forman sumando, con distintas intensidades, tres componentes
//       básicos: el rojo (r=red), el verde (g=green) y el azul (b=blue).
//
//       Cada componente r, g, b es un número entre 0 y 255 de manera que 0 significa
//       que la componente no influye en la mezcla y 255 que influye todo lo que
//       pueda influir. Valores intermedios indican que la componente se mezcla, en
//       menor grado, con las otras componentes.
//
//       Por ejemplo, el color negro se obtiene cuando no 'encendemos' ninguna de
//       las componentes, es decir, la tripleta (0,0,0) dibuja el color negro (todas
//       las componentes a la mínima intensidad).
//
//       El color blanco se obtiene encendiendo las tres componentes con la máxima
//       intensidad, es decir, (255,255,255).
//       Si queremos un rojo puro: (255,0,0)
//       Si queremos un rojo menos intenso: (100,0,0) o bien (150,0,0) ... cambiaremos
//       el valor de la componente R según la intensidad de rojo que deseemos.
//
//       El amarillo se obtiene mezclando rojo y verde. Según las proporciones
//       obtendremos amarillos más puros, más intensos, más apagados, etc ...
//       Por ejemplo (255,255,0) o bien (150,230,0), ...
//
//       De esta forma podemos obtener el espectro completo de colores, en total
//       podemos crear un total de 256*256*256=16.777.216 colores distintos.
