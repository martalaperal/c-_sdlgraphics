
/* ***************************************** */

#ifndef __GRAFICOS__H__
#define __GRAFICOS__H__

/* ***************************************** */

#include "SDL/SDL.h"
#include "SDL/SDL_gfxPrimitives.h"

/* ***************************************** */

// Inicializar la biblioteca SDL y crear una ventana
void CrearVentana(int ancho, int alto, const char *titulo);

// Consultar Dimensiones de Ventana
int FilasVentana();
int ColumnasVentana();
// Borrar la Ventana
void LimpiarVentana();
// Dibujo de formas básicas
void Punto(int x, int y, unsigned char r, unsigned char g, unsigned char b);
void Linea(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b);
void Circulo(int x, int y, int rad, unsigned char r, unsigned char g, unsigned char b);
void Rectangulo(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b);
void Elipse(int x, int y, int radx, int rady, unsigned char r, unsigned char g, unsigned char b);

// Formas rellenas de color
void RectanguloR(int x1, int y1, int x2, int y2, unsigned char r, unsigned char g, unsigned char b);
void CirculoR(int x, int y, int rad, unsigned char r, unsigned char g, unsigned char b);
void ElipseR(int x, int y, int radx, int rady, unsigned char r, unsigned char g, unsigned char b);
void Texto(int x, int y, const char *c,  unsigned char r, unsigned char g, unsigned char b);

// Obtener el color de un pixel de la pantalla
void ObtenerPunto(int x, int y, unsigned char &r, unsigned char &g, unsigned char &b);

// Cargar desde un fichero en disco una imagen BMP
SDL_Surface *LeerImagenBMP(const char *fich);
// Liberar la memoria ocupada por una imagen BMP
void LiberarImagenBMP(SDL_Surface *img);
// Dimensiones de una imagen BMP
void DimensionesBMP(const char *imagen, int& x, int& y);
// Dibujar en las coordenadas de pantalla (x,y) la imagen BMP
void DibujarImagenBMP(SDL_Surface *img, int x, int y);

// Obtener el evento de click del ratón
int ObtenerClick(int &x, int &y);

// Obtener el evento de pulsación de una tecla
int ObtenerTecla();

// Espera durante ms milisegundos
void Esperar(int ms);

/* ***************************************** */

#endif

/* ***************************************** */
